--
-- PostgreSQL database dump
--

-- Dumped from database version 16.2 (Debian 16.2-1.pgdg120+2)
-- Dumped by pg_dump version 16.2 (Debian 16.2-1.pgdg120+2)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: wb0schema; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA wb0schema;


ALTER SCHEMA wb0schema OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: item; Type: TABLE; Schema: wb0schema; Owner: postgres
--

CREATE TABLE wb0schema.item (
    chrt_id integer NOT NULL,
    track_number character varying(50),
    price integer NOT NULL,
    rid character varying(50) NOT NULL,
    item_name character varying(50) NOT NULL,
    sale integer NOT NULL,
    item_size character varying(50) NOT NULL,
    total_price integer NOT NULL,
    nm_id integer NOT NULL,
    brand character varying(50) NOT NULL,
    item_status integer NOT NULL
);


ALTER TABLE wb0schema.item OWNER TO postgres;

--
-- Name: order; Type: TABLE; Schema: wb0schema; Owner: postgres
--

CREATE TABLE wb0schema."order" (
    order_uid character varying(50) NOT NULL,
    track_number character varying(50) NOT NULL,
    order_entry character varying(50) NOT NULL,
    locale character varying(50) NOT NULL,
    internal_signature character varying(50) NOT NULL,
    customer_id character varying(50) NOT NULL,
    delivery_service character varying(50) NOT NULL,
    shard_key character varying(50) NOT NULL,
    sm_id integer NOT NULL,
    date_created character varying(50) NOT NULL,
    oof_shard character varying(50) NOT NULL,
    delivery_name character varying(50) NOT NULL,
    delivery_phone character varying(50) NOT NULL,
    delivery_zip character varying(50) NOT NULL,
    delivery_city character varying(50) NOT NULL,
    delivery_address character varying(50) NOT NULL,
    delivery_region character varying(50) NOT NULL,
    delivery_email character varying(50) NOT NULL,
    payment_transaction character varying(50),
    payment_request_id character varying(50) NOT NULL,
    payment_currency character varying(50) NOT NULL,
    payment_provider character varying(50) NOT NULL,
    payment_amount integer NOT NULL,
    payment_payment_dt integer NOT NULL,
    payment_bank character varying(50) NOT NULL,
    payment_delivery_cost integer NOT NULL,
    payment_goods_total integer NOT NULL,
    payment_custom_fee integer NOT NULL
);


ALTER TABLE wb0schema."order" OWNER TO postgres;

--
-- Data for Name: item; Type: TABLE DATA; Schema: wb0schema; Owner: postgres
--

COPY wb0schema.item (chrt_id, track_number, price, rid, item_name, sale, item_size, total_price, nm_id, brand, item_status) FROM stdin;
\.


--
-- Data for Name: order; Type: TABLE DATA; Schema: wb0schema; Owner: postgres
--

COPY wb0schema."order" (order_uid, track_number, order_entry, locale, internal_signature, customer_id, delivery_service, shard_key, sm_id, date_created, oof_shard, delivery_name, delivery_phone, delivery_zip, delivery_city, delivery_address, delivery_region, delivery_email, payment_transaction, payment_request_id, payment_currency, payment_provider, payment_amount, payment_payment_dt, payment_bank, payment_delivery_cost, payment_goods_total, payment_custom_fee) FROM stdin;
\.


--
-- Name: item item_pkey; Type: CONSTRAINT; Schema: wb0schema; Owner: postgres
--

ALTER TABLE ONLY wb0schema.item
    ADD CONSTRAINT item_pkey PRIMARY KEY (chrt_id);


--
-- Name: order order_pkey; Type: CONSTRAINT; Schema: wb0schema; Owner: postgres
--

ALTER TABLE ONLY wb0schema."order"
    ADD CONSTRAINT order_pkey PRIMARY KEY (order_uid);


--
-- PostgreSQL database dump complete
--

