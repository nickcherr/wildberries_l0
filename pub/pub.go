package main

import (
	"encoding/json"
	"log"
	"math/rand"
	"strconv"

	"github.com/brianvoe/gofakeit"
	"github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"
)

type Delivery struct {
	Name    string
	Phone   string
	Zip     string
	City    string
	Address string
	Region  string
	Email   string
}

type Payment struct {
	Transaction   string
	Request_id    string
	Currency      string
	Provider      string
	Amount        int
	Payment_dt    int
	Bank          string
	Delivery_cost int
	Goods_total   int
	Custom_fee    int
}

type Item struct {
	Chrt_id      int
	Track_number string
	Price        int
	Rid          string
	Name         string
	Sale         int
	Size         string
	Total_price  int
	Nm_id        int
	Brand        string
	Status       int
}

type Order struct {
	Order_uid          string   `json:"order_uid"`
	Track_number       string   `json:"track_number"`
	Entry              string   `json:"entry"`
	Delivery           Delivery `json:"delivery"`
	Payment            Payment  `json:"payment"`
	Items              []Item   `json:"items"`
	Locale             string   `json:"locale"`
	Internal_signature string   `json:"internal_signature"`
	Customer_id        string   `json:"customer_id"`
	Delivery_service   string   `json:"delivery_service"`
	Shardkey           string   `json:"shardkey"`
	Sm_id              int      `json:"sm_id"`
	Date_created       string   `json:"date_created"`
	Oof_shard          string   `json:"oof_shard"`
}

var symbols = []rune("abcdefghijklmnopqrstuvwxyz1234567890")

func randSeq(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = symbols[rand.Intn(len(symbols))]
	}
	return string(b)
}

func generateOrder() []byte {
	order := Order{}

	order.Order_uid = randSeq(20)
	order.Track_number = randSeq(15)
	order.Entry = randSeq(5)
	order.Locale = "ru"
	order.Internal_signature = ""
	order.Customer_id = randSeq(4)
	order.Delivery_service = randSeq(5)
	order.Shardkey = randSeq(1)
	order.Sm_id = rand.Intn(100)
	order.Date_created = "2021-11-26T06:22:19Z"
	order.Oof_shard = randSeq(1)

	order.Delivery.Name = gofakeit.Name()
	order.Delivery.Phone = gofakeit.Phone()
	order.Delivery.Zip = strconv.Itoa(int(rand.Int31()))
	order.Delivery.City = gofakeit.City()
	order.Delivery.Address = gofakeit.StreetName() + " " + strconv.Itoa(1+rand.Intn(50))
	order.Delivery.Region = gofakeit.State()
	order.Delivery.Email = gofakeit.Email()

	goods_total := 0
	numItems := 1 + rand.Intn(5)
	for i := 0; i < numItems; i++ {
		item := Item{}
		item.Chrt_id = rand.Intn(10000000)
		item.Track_number = order.Track_number
		item.Price = 200 + rand.Intn(3000)
		item.Rid = randSeq(20)
		item.Name = "item name"
		item.Sale = 0
		item.Size = "0"
		item.Total_price = item.Price
		item.Nm_id = rand.Intn(10000000)
		item.Brand = gofakeit.Company()
		item.Status = 202
		order.Items = append(order.Items, item)
		goods_total += item.Total_price
	}

	order.Payment.Transaction = order.Order_uid
	order.Payment.Request_id = ""
	order.Payment.Currency = "RUB"
	order.Payment.Provider = randSeq(5)
	order.Payment.Payment_dt = rand.Intn(2147483647)
	order.Payment.Bank = randSeq(7)
	order.Payment.Delivery_cost = 100 + rand.Intn(400)
	order.Payment.Goods_total = goods_total
	order.Payment.Custom_fee = 0
	order.Payment.Amount = order.Payment.Delivery_cost + goods_total
	order.Payment.Amount += order.Payment.Custom_fee

	bytes, _ := json.Marshal(order)
	return bytes
}

func main() {
	URL := stan.DefaultNatsURL
	clusterID := "test-cluster"
	clientID := "stan-pub"
	opts := []nats.Option{nats.Name("NATS Streaming Subscriber")}

	nc, err := nats.Connect(URL, opts...)
	if err != nil {
		log.Fatal(err)
	}
	defer nc.Close()

	sc, err := stan.Connect(clusterID, clientID, stan.NatsConn(nc),
		stan.SetConnectionLostHandler(func(_ stan.Conn, reason error) {
			log.Fatalf("Connection lost, reason: %v", reason)
		}))
	if err != nil {
		log.Fatalf("Can't connect: %v.\nMake sure a NATS Streaming Server is running at: %s", err, URL)
	}
	defer sc.Close()

	for i := 0; i < 10; i++ {
		msg := generateOrder()
		err = sc.Publish("stan_channel", msg)
		if err != nil {
			log.Fatalf("Error during publish: %v\n", err)
		}
	}
}
