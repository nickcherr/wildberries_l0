CREATE SCHEMA IF NOT EXISTS wb0schema;

CREATE TABLE IF NOT EXISTS wb0schema.order (
    order_uid varchar(50) PRIMARY KEY NOT NULL,
    track_number varchar(50) NOT NULL,
    order_entry varchar(50) NOT NULL,
    locale varchar(50) NOT NULL,
    internal_signature varchar(50) NOT NULL,
    customer_id varchar(50) NOT NULL,
    delivery_service varchar(50) NOT NULL,
    shard_key varchar(50) NOT NULL,
    sm_id int NOT NULL,
    date_created varchar(50) NOT NULL,
    oof_shard varchar(50) NOT NULL,
    delivery_name varchar(50) NOT NULL,
    delivery_phone varchar(50) NOT NULL,
    delivery_zip varchar(50) NOT NULL,
    delivery_city varchar(50) NOT NULL,
    delivery_address varchar(50) NOT NULL,
    delivery_region varchar(50) NOT NULL,
    delivery_email varchar(50) NOT NULL,
    payment_transaction varchar(50),
    payment_request_id varchar(50) NOT NULL,
    payment_currency varchar(50) NOT NULL,
    payment_provider varchar(50) NOT NULL,
    payment_amount int NOT NULL,
    payment_payment_dt int NOT NULL,
    payment_bank varchar(50) NOT NULL,
    payment_delivery_cost int NOT NULL,
    payment_goods_total int NOT NULL,
    payment_custom_fee int NOT NULL
);

CREATE TABLE IF NOT EXISTS wb0schema.item (
    chrt_id int NOT NULL PRIMARY KEY,
    track_number varchar(50),
    price int NOT NULL,
    rid varchar(50) NOT NULL,
    item_name varchar(50) NOT NULL,
    sale int NOT NULL,
    item_size varchar(50) NOT NULL,
    total_price int NOT NULL,
    nm_id int NOT NULL,
    brand varchar(50) NOT NULL,
    item_status int NOT NULL
);
