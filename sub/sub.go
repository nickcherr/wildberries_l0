package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"

	"github.com/gorilla/mux"
	nats "github.com/nats-io/nats.go"
	stan "github.com/nats-io/stan.go"

	_ "github.com/lib/pq"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "1q2w3e4r"
	dbname   = "wb0"
)

var (
	cache Cache
	db    *sql.DB
)

type Cache struct {
	Orders []Order
	Mu     sync.Mutex
}

type Delivery struct {
	Name    string `json:"name"`
	Phone   string `json:"phone"`
	Zip     string `json:"zip"`
	City    string `json:"city"`
	Address string `json:"address"`
	Region  string `json:"region"`
	Email   string `json:"email"`
}

type Payment struct {
	Transaction   string `json:"transaction"`
	Request_id    string `json:"request_id"`
	Currency      string `json:"currency"`
	Provider      string `json:"provider"`
	Amount        int    `json:"amount"`
	Payment_dt    int    `json:"payment_dt"`
	Bank          string `json:"bank"`
	Delivery_cost int    `json:"delivery_cost"`
	Goods_total   int    `json:"goods_total"`
	Custom_fee    int    `json:"custom_fee"`
}

type Item struct {
	Chrt_id      int    `json:"chrt_id"`
	Track_number string `json:"track_number"`
	Price        int    `json:"price"`
	Rid          string `json:"rid"`
	Name         string `json:"name"`
	Sale         int    `json:"sale"`
	Size         string `json:"size"`
	Total_price  int    `json:"total_price"`
	Nm_id        int    `json:"nm_id"`
	Brand        string `json:"brand"`
	Status       int    `json:"status"`
}

type Order struct {
	Order_uid          string   `json:"order_uid"`
	Track_number       string   `json:"track_number"`
	Entry              string   `json:"entry"`
	Delivery           Delivery `json:"delivery"`
	Payment            Payment  `json:"payment"`
	Items              []Item   `json:"items"`
	Locale             string   `json:"locale"`
	Internal_signature string   `json:"internal_signature"`
	Customer_id        string   `json:"customer_id"`
	Delivery_service   string   `json:"delivery_service"`
	Shardkey           string   `json:"shardkey"`
	Sm_id              int      `json:"sm_id"`
	Date_created       string   `json:"date_created"`
	Oof_shard          string   `json:"oof_shard"`
}

type Handler struct {
	DB   *sql.DB
	Tmpl *template.Template
}

func AddToCache(order Order) {
	cache.Mu.Lock()
	cache.Orders = append(cache.Orders, order)
	cache.Mu.Unlock()
}

func processMsg(msg *stan.Msg) {
	newOrder := jsonToOrder(msg.Data)
	putOrderInDB(newOrder)
}

func jsonToOrder(bytes []byte) Order {
	var newOrder Order
	err := json.Unmarshal(bytes, &newOrder)
	if err != nil {
		return Order{}
	}
	return newOrder
}

func putOrderInDB(newOrder Order) {
	_, err := db.Exec(
		`INSERT INTO wb0schema.order (
			order_uid, track_number, order_entry, locale, internal_signature, customer_id, delivery_service, 
			shard_key, sm_id, date_created, oof_shard, delivery_name, delivery_phone, delivery_zip, delivery_city, 
			delivery_address, delivery_region, delivery_email, payment_transaction, payment_request_id,
			payment_currency, payment_provider, payment_amount, payment_payment_dt, payment_bank,
			payment_delivery_cost, payment_goods_total, payment_custom_fee)
		VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22,$23,$24,$25,$26,$27,$28)`,
		newOrder.Order_uid, newOrder.Track_number, newOrder.Entry, newOrder.Locale, newOrder.Internal_signature,
		newOrder.Customer_id, newOrder.Delivery_service, newOrder.Shardkey, newOrder.Sm_id, newOrder.Date_created,
		newOrder.Oof_shard, newOrder.Delivery.Name, newOrder.Delivery.Phone, newOrder.Delivery.Zip,
		newOrder.Delivery.City, newOrder.Delivery.Address, newOrder.Delivery.Region, newOrder.Delivery.Email,
		newOrder.Payment.Transaction, newOrder.Payment.Request_id, newOrder.Payment.Currency,
		newOrder.Payment.Provider, newOrder.Payment.Amount, newOrder.Payment.Payment_dt, newOrder.Payment.Bank,
		newOrder.Payment.Delivery_cost, newOrder.Payment.Goods_total, newOrder.Payment.Custom_fee,
	)

	if err != nil {
		return
	}

	cache.Orders = append(cache.Orders, newOrder) // only if added successfully to db

	for _, item := range newOrder.Items {
		_, err = db.Exec(
			`INSERT INTO wb0schema.item (
				chrt_id, track_number, price, rid, item_name, sale, item_size, total_price, nm_id, brand, item_status
			)
			VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11)`,
			item.Chrt_id, item.Track_number, item.Price, item.Rid, item.Name, item.Sale,
			item.Size, item.Total_price, item.Nm_id, item.Brand, item.Status,
		)
		if err != nil {
			return
		}
	}
}

func getOrderByID(ID string) []byte {
	for _, order := range cache.Orders {
		if order.Order_uid == ID {
			bytes, err := json.MarshalIndent(order, "", "  ")
			if err != nil {
				panic(err)
			}
			return bytes
		}
	}

	return []byte("No order with such ID\n")
}

func getCacheFromDB() {
	rows, err := db.Query(
		`SELECT 
			order_uid, track_number, order_entry, locale, internal_signature, customer_id, delivery_service, 
			shard_key, sm_id, date_created, oof_shard, delivery_name, delivery_phone, delivery_zip, delivery_city, 
			delivery_address, delivery_region, delivery_email, payment_transaction, payment_request_id,
			payment_currency, payment_provider, payment_amount, payment_payment_dt, payment_bank,
			payment_delivery_cost, payment_goods_total, payment_custom_fee
		FROM wb0schema.order`)

	if err != nil {
		log.Fatalf("Unable to get cache from database: %v", err)
	}
	for rows.Next() {
		order := Order{}
		err = rows.Scan(
			&order.Order_uid, &order.Track_number, &order.Entry, &order.Locale, &order.Internal_signature, &order.Customer_id,
			&order.Delivery_service, &order.Shardkey, &order.Sm_id, &order.Date_created, &order.Oof_shard, &order.Delivery.Name,
			&order.Delivery.Phone, &order.Delivery.Zip, &order.Delivery.City, &order.Delivery.Address, &order.Delivery.Region,
			&order.Delivery.Email, &order.Payment.Transaction, &order.Payment.Request_id, &order.Payment.Currency,
			&order.Payment.Provider, &order.Payment.Amount, &order.Payment.Payment_dt, &order.Payment.Bank,
			&order.Payment.Delivery_cost, &order.Payment.Goods_total, &order.Payment.Custom_fee,
		)
		if err != nil {
			panic(err)
		}
		items, err := db.Query(
			`SELECT 
				chrt_id, track_number, price, rid, item_name, sale, item_size, total_price, nm_id, brand, item_status
			FROM wb0schema.item
			WHERE track_number = $1`, order.Track_number)
		if err != nil {
			panic(err)
		}
		for items.Next() {
			item := Item{}
			err = items.Scan(
				&item.Chrt_id, &item.Track_number, &item.Price, &item.Rid, &item.Name, &item.Sale,
				&item.Size, &item.Total_price, &item.Nm_id, &item.Brand, &item.Status,
			)
			if err != nil {
				panic(err)
			}
			order.Items = append(order.Items, item)
		}

		items.Close()
		cache.Orders = append(cache.Orders, order)
	}
	rows.Close()
}

func main() {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)

	var err error
	db, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}

	db.SetMaxOpenConns(10)

	err = db.Ping()
	if err != nil {
		panic(err)
	}

	getCacheFromDB()

	URL := stan.DefaultNatsURL
	clusterID := "test-cluster"
	clientID := "stan-sub"
	opts := []nats.Option{nats.Name("NATS Streaming Subscriber")}
	nc, err := nats.Connect(URL, opts...)
	if err != nil {
		log.Fatal(err)
	}
	defer nc.Close()

	sc, err := stan.Connect(clusterID, clientID, stan.NatsConn(nc),
		stan.SetConnectionLostHandler(func(_ stan.Conn, reason error) {
			log.Fatalf("Connection lost, reason: %v", reason)
		}))
	if err != nil {
		log.Fatalf("Can't connect: %v.\nMake sure a NATS Streaming Server is running at: %s", err, URL)
	}
	defer sc.Close()

	startOpt := stan.StartWithLastReceived() //?
	mcb := func(msg *stan.Msg) {
		processMsg(msg)
	}

	_, err = sc.QueueSubscribe("stan_channel", "stan_group", mcb, startOpt, stan.DurableName("durable_name"))
	if err != nil {
		sc.Close()
		log.Fatal(err)
	}

	signalChan := make(chan os.Signal, 1)
	cleanupDone := make(chan bool)
	signal.Notify(signalChan, os.Interrupt)
	go func() {
		for range signalChan {
			fmt.Printf("\nReceived an interrupt, unsubscribing and closing connection...\n\n")
			sc.Close()
			cleanupDone <- true
		}
	}()

	handlers := &Handler{
		DB:   db,
		Tmpl: template.Must(template.ParseGlob("templates/*")),
	}

	r := mux.NewRouter()
	r.HandleFunc("/", handlers.Form).Methods("GET")

	fmt.Println("starting server at :8080")
	fmt.Println(http.ListenAndServe(":8080", r))

	<-cleanupDone
}

func (h *Handler) Form(w http.ResponseWriter, r *http.Request) {
	err := h.Tmpl.ExecuteTemplate(w, "form.html", nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	uid := r.FormValue("UID")
	order := getOrderByID(uid)
	w.Write([]byte("<pre>" + string(order) + "</pre>"))
}
